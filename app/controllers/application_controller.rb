class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  #faz esses métodos estarem disponíveis para todas as views.
  helper_method :current_user, :logged_in? 


  def current_user
  	#retorne o User caso haja um guardado no session do browser e devolva o user.id
  	@current_user ||= User.find(session[:user_id]) if session[:user_id] 
  end

  def logged_in?
  	!!current_user # o sinal !! transforma em boolean
  end

  def require_user
  	if !logged_in?
  		flash[:danger] = "You must be logged in to perform that action"
  		redirect_to root_path
  	end
  end
end
